float x, y;
float xSpeed = 10;
float ySpeed = 10;

void setup() 
{
  size(1920, 1080, P3D);
  x = Window.windowWidth;
  y = Window.windowHeight;
}
    
void draw() 
{
  background(0);
  
  x += xSpeed;
  y += ySpeed;
  
  ellipse(x, y, 50, 50);
  
  if (x == Window.widthPx || x == 0)
  {
     xSpeed *= -1;
  }
  
  if (y == Window.heightPx || y == 0)
  {
    ySpeed *= -1;
  }
}
