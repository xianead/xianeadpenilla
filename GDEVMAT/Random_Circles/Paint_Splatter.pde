class Paint_Splatter
{
  float xPosition;
  float yPosition;

  Paint_Splatter()
  {
    xPosition = 0;
    yPosition = 0;
  }

  Paint_Splatter(float x, float y)
  {
    xPosition = x;
    yPosition = y;
  }

  void render()
  {
    int randomR = floor(random(256));
    int randomG = floor(random(256));
    int randomB = floor(random(256));
    int randomA = floor(random(10, 100));  

    float xLocation = generateGaussianNumber(300);
    float circleSize = generateGaussianNumber(50);

    noStroke();
    fill(randomR, randomG, randomB, randomA);
    circle(xLocation, random(-yPosition, yPosition), circleSize);
  }

  float generateGaussianNumber(int sd)
  {
    float value = (float) randomGaussian();
    float standardDeviation = sd;
    float mean = xPosition / 2;
    value = (value * standardDeviation) + mean;

    return value;
  }
}
