void setup()
{
// Sets the screen resolution and render setting
size(1920, 1080, P3D);

// Sets the origin to the middle 
camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);

// Sets the background
background(255);
}

Paint_Splatter randomPaint = new Paint_Splatter(0, 1080);

int frame = 0;
void draw()
{
frame++;

if (frame >= 60)
{
frame = 0;
background(255);
}

randomPaint.render();
}
