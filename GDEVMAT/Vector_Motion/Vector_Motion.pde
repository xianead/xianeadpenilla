Mover mover = new Mover();

void setup()
{
  // Sets the screen resolution and render setting
  size(1920, 1080, P3D);

  // Sets the origin to the middle 
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);

  // Sets the background
  background(0);

  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1, 0);
}

boolean decelerate = false;
void draw()
{
  background(255);

  noStroke();

  mover.update();
  mover.setColor(100, 50, 25, 100);

  if (mover.position.x > 0. && mover.velocity.x > 0)
  {
    decelerate = true;
  }
  
  if (decelerate)
  {
    mover.acceleration.x -= 0.1;
    if (mover.acceleration.x <= 0.0)
    {
      decelerate = false;
    }
  }
 
}
