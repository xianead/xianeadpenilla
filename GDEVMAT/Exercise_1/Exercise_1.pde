int diameter = 16;
float t = 0.0;
float dt = .05;
float amplitude = 100.0;
float frequency = 0.7;

void setup() 
{
  // Gets called when the program runs
  
  size(1920, 1080, P3D);
  
  // Sets the origin to the middle 
  //camera(0,0, -(height / 2) / tan(PI * 30 / 180),
  //       0, 0, 0,
  //       0, -1, 0);
}

void draw() 
{
  // Gets called every frame
  
  background(0);
  
  // drawCartesianPlane(500);
  // drawLinearFunction(300);
  // drawQuadraticFunction(300);
  // drawCircle();
  
  renderWave();
  
}

void drawCartesianPlane(int size)
{
   line(size, 0, -size, 0);
   line(0, size, 0, -size);
   
   for ( int i = -size; i <= size; i += 10)
   {
     line(i, -5, i, 5);
     line(-5, i, 5, i);
   }  
}

void drawLinearFunction(int size)
{
  /*
   
   f(x) = x + 2;
   Let x be 4, then y = 6 (4, 6)
   Let x be -5, then y = -3 (-5. 3)
  
  */
  
  for (int x = -size; x <= size; x ++)
  {
    circle(x, x + 2, 1);
  }
}

void drawQuadraticFunction(int size)
{
  /*
  
  f(x) = x^2 + 2x - 5
  Let x be 2, then y = 3
  Let x be -2, then y = 3
  Let x be -1, then y = -6
  
  */ 
  
  for (float x = -size; x <= size; x += 0.1)
  {
    circle(x * 10, (x * x) + (2 * x) - 5, 1);
  }
}

float radius = 50;

void drawCircle()
{
  for (int x = 0; x < 360; x++)
  {
     circle((float) Math.cos(x) * radius, (float) Math.sin(x) * radius, 1); 
  }
  
  radius++;
}

void renderWave()
{
  for (int i = 0; i < width/diameter + 10; i++)
  {
     ellipse(i * diameter, amplitude * sin(frequency * (t+i)) + height / 2, diameter, diameter);
  }
  t += dt;
  
}

void keyPressed()
{
   if (key == 'w' || key == 'W')
   {
     amplitude += 5;
   }
   if (key == 'a' || key == 'A')
   {
      frequency += 0.1;
   }
   if (key == 's' || key == 'S')
   {
     amplitude -= 5;
   }
   if (key == 'd' || key == 'D')
   {
      frequency -= 0.1;
   }
}
