public class Bouncing_Ball
{

  private Vector2 position = new Vector2();
  private Vector2 velocity = new Vector2(5, 8);

  void render()
  {

    fill(200, 0, 150);
    circle(position.x, position.y, 50);

    position.add(velocity);

    if (position.x > Window.right || position.x < Window.left)
    {
      velocity.x *= -1;
    }
    if (position.y > Window.top || position.y < Window.bottom)
    {
      velocity.y *= -1;
    }
  }
}
