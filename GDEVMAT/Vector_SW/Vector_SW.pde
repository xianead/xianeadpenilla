void setup() 
{
  // Sets the screen resolution and render setting
  size(1920, 1080, P3D);

  // Sets the origin to the middle 
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);

  // Sets the background
  background(0);
}

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);

  return new Vector2(x, y);
}

float dt = 0;
float r_t = 100;
float g_t = 150; 
float b_t = 200;

void saber(float radian, int mult, int strokeWeight, PVector rgb)
{
  strokeWeight(strokeWeight);
  stroke(rgb.x, rgb.y, rgb.z);
  rotate(radian);
  line(0, 0, mult, mult); 
  rotate(-radian);
}

void draw()
{
  background(0);

  // Vector2 mouse = mousePos();

  // Rotation variables
  float n = noise(dt);
  float n_m = map(n, 0, 1, 0, TWO_PI);

  // Color
  float red = noise(r_t);
  float red_m = map(red, 0, 1, 0, 255);

  float green = noise(g_t);
  float green_m = map(green, 0, 1, 0, 255);

  float blue = noise(b_t);
  float blue_m = map(blue, 0, 1, 0, 255);

  PVector saberColor = new PVector(red_m, green_m, blue_m);
  PVector handle = new PVector(170, 170, 170);

  rotate(n_m);
  
  // Handle
  saber(-PI, 70, 12, handle);
  
  // Main saber
  saber(0, 300, 10, saberColor);

  // Right saber
  saber(1.5708, 50, 10, saberColor);

  // Left saber
  saber(-1.5708, 50, 10, saberColor);

  // Delta time increments
  dt += 0.01;
  r_t += 0.01;
  g_t += 0.01; 
  b_t += 0.01;

  println(n_m);
}
