void setup()
{
  // Sets the screen resolution and render setting
  size(1920, 1080, P3D);

  // Sets the origin to the middle 
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);

  // Sets the background
  background(255);
}

float dt = 0; //delta time

void draw()
{
  float n = noise(dt);
  float x = map(n, 0, 1, 0, Window.top);
  
  rect(Window.left + (dt * 100), Window.bottom, 1, x);

  dt += 0.01f;
}
