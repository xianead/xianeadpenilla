int arraySize = 200;
Mover blackHole;
Mover circles[] = new Mover[arraySize];

void setup()
{
  // Sets the screen resolution and render setting
  size(1920, 1080, P3D);

  // Sets the origin to the middle 
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);

  // Sets the background
  background(0);

  createCircles();
}

float generateGaussianNumber(float sdValue, float meanValue)
{
  float value = randomGaussian();

  value = (value * sdValue) + meanValue;

  return value;
}

void createCircles()
{
  for (int i = 0; i < 200; i++)
  {
    float x = generateGaussianNumber(50, random(Window.left, Window.right));
    float y = generateGaussianNumber(50, random(Window.bottom, Window.top));

    float red = map(noise(random(255)), 0, 1, 0, 255);
    float green = map(noise(random(255)), 0, 1, 0, 255);
    float blue = map(noise(random(255)), 0, 1, 0, 255);

    circles[i] = new Mover(x, y, random(20, 45)); 
    circles[i].setColor(red, green, blue, 255);
  }

  blackHole = new Mover(random(Window.left, Window.right), random(Window.bottom, Window.top), 50);
}

int frames = 0;

void draw()
{
  background(0);

  frames++;

  if (frames >= 200)
  {
    frames = 0;
    createCircles();
  }

  for (int i = 0; i < arraySize; i++)
  {
    noStroke();
    circles[i].render();
    PVector direction = PVector.sub(blackHole.position, circles[i].position);
    direction.normalize();
    direction.mult(10);

    circles[i].position.add(direction);

    blackHole.render();
  }
}
