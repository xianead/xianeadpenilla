float dt = 0;
float dt_2 = 100;
float r_t = 100;
float g_t = 150; 
float b_t = 200;
float size_t = 0;

class Walker
{
  float xPosition;
  float yPosition;

  Walker()
  {
    xPosition = 0;
    yPosition = 0;
  }

  Walker(float x, float y)
  {
    xPosition = x;
    yPosition = y;
  }

  void render()
  {
    // Position
    float n = noise(dt);
    float n_m = map(n, 0, 1, Window.left, Window.right);
    
    float n_2 = noise(dt_2);
    float y_m = map(n_2, 0, 1, Window.bottom, Window.top);

    // Color
    float red = noise(r_t);
    float red_m = map(red, 0, 1, 0, 255);
    
    float green = noise(g_t);
    float green_m = map(green, 0, 1, 0, 255);
      
    float blue = noise(b_t);
    float blue_m = map(blue, 0, 1, 0, 255);

    // Size
    float size = noise(size_t);
    float size_m = map(size, 0, 1, 10, 250);

    dt += 0.01;
    dt_2 += 0.01;
    r_t += 0.01;
    g_t += 0.01; 
    b_t += 0.01;
    size_t += 0.01;

    fill(red_m, green_m, blue_m);
    circle(n_m, y_m, size_m);
  }
}
