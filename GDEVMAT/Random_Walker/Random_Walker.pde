void setup() 
{
  // Gets called when the program runs
  
  // Sets the screen resolution and render setting
  size(1920, 1080, P3D);
  
  // Sets the origin to the middle 
  camera(0,0, -(height / 2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  
  // Sets the background
  background(0);
}

Walker walker = new Walker();

void draw()
{
  walker.render();
}
