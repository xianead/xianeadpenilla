#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>

#include "UnorderedArray.h"

using namespace std;

int main()
{
	UnorderedArray<int> grades(5);
	
	for (int i = 0; i < 10; i++)
		grades.push(i);

	cout << "Initial Values: " << endl;
	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	cout << "\nRemove index 1" << endl;
	grades.remove(1);

	cout << "\n\nAfter removing index 1" << endl;
	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	cout << "What element are you looking for? " << endl;
	int element;
	cin >> element;
	cout << "Index: " << grades.linearSearch(element) << endl;


	_getch();
}