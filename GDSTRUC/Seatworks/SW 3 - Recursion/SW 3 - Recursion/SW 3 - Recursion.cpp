#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

int findSum(int number)
{
	if (number <= 0)
		return 0;

	return findSum(number / 10) + number % 10;
}

int fibonacci(int number)
{
	if (number == 0 || number == 1)
		return number;
	else
		return (fibonacci(number - 1) + fibonacci(number - 2));
}

bool isPrime(int number, int divisor = 2)
{
	if (number < 3)
		return false;

	if (number % divisor == 0)
		return false;

	if (divisor * divisor > number)
		return true;

	return isPrime(number, divisor + 1);
}

int main()
{
	int numbersToAdd, fib;
	int start = 0;
	int prime;

	cout << "1. Compute the sum of digits of a number example" << endl;
	cout << "Input: ";
	cin >> numbersToAdd;
	cout << "Output: " << findSum(numbersToAdd) << "\n\n";

	cout << "2. Print fibonacci numbers up to nth term" << endl;
	cout << "Input: ";
	cin >> fib;
	for (int i = 1; i <= fib; i++)
	{
		cout << fibonacci(start) << " ";
		start++;
	}
	cout << "\n\n";

	cout << "3. Check a number whether it is prime or not" << endl;
	cout << "Input: ";
	cin >> prime;
	if (isPrime(prime))
		cout << "True";
	else
		cout << "False";

}