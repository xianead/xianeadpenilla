#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

void printMemberList(string* guildArray, int userInput)
{
	for (int i = 0; i < userInput; i++)
	{
		cout << "Member # " << i + 1 << ": " << guildArray[i] << endl;
	}
}

string* returnNewArray(string* currentArray, int choice, int userInput)
{
	int size = choice + userInput;

	string* newArray = new string[size];

	for (int i = 0; i < userInput; i++)
	{
		newArray[i] = currentArray[i];
	}

	return newArray;
}

int main()
{
	int userInput;
	bool active = 1;
	string* guildArray;
	string guildName;

	cout << "Please enter guild name: ";
	cin.ignore();
	getline(cin, guildName);

	cout << "Please enter guild size: ";
	cin >> userInput;
	guildArray = new string[userInput];

	cout << "\nPlease enter the names of your members !! " << endl;
	cout << endl;
	cin.ignore();

	for (int i = 0; i < userInput; i++)
	{
		cout << "Enter member # " << i + 1 << ": ";
		getline(cin, guildArray[i]);
	} 

	cout << endl;
	cout << "Member List: " << endl;

	for (int i = 0; i < userInput; i++)
	{
		cout << "Member # " << i + 1 << ": " << guildArray[i] << endl;
	}
	_getch();

	char userChoice;

	while (active)
	{
		system("cls");

		cout << "What do you want to do?" << endl;
		cout << "\n[1] Print every member\n"
			<< "[2] Rename a member\n"
			<< "[3] Add a member\n"
			<< "[4] Delete a member\n"
			<< "[5] End program"
			<< endl;

		cout << "\nYour answer: ";
		cin >> userChoice;

		if (userChoice == '1')
		{
			printMemberList(guildArray, userInput);
			_getch();
		}
		else if (userChoice == '2')
		{
			system("cls");
			bool active = 1;
			int renameChoice;

			while (active)
			{
				cout << "Which member do you want to rename? " << endl;
				cout << endl;

				printMemberList(guildArray, userInput);

				cout << endl;
				cout << "Your choice: ";
				cin >> renameChoice;

				if ((renameChoice < 1) || (renameChoice > userInput))
				{
					cout << "Try agan!!" << endl;
					system("cls");
				}
				else
					break;
			}
			cout << "New name: ";
			cin.ignore();
			getline(cin, guildArray[renameChoice - 1]);

			printMemberList(guildArray, userInput);

			_getch();
		}
		else if (userChoice == '3')
		{
			system("cls");
			int tempSize = userInput;
			int choice;

			cout << "How many do you want to add? ";
			cin >> choice;

			string* tempArray = returnNewArray(guildArray, choice, tempSize);

			userInput += choice;

			delete[] guildArray;

			guildArray = tempArray;

			cout << "Please enter new members' names: " << endl;
			cout << endl;
			cin.ignore();

			// Takes the name of the new members
			for (int i = tempSize; i < userInput; i++)
			{
				cout << "Member # " << i + 1 << ": ";
				getline(cin, guildArray[i]);
			}

			cout << endl;

			printMemberList(guildArray, userInput);

			_getch();
		}
		else if (userChoice == '4')
		{
			system("cls");

			int index;

			printMemberList(guildArray, userInput);
			cout << endl;

			do
			{
				cout << "Which member do you want to remove?\n" << "Your answer: ";
				cin >> index;
			} while (index > userInput || index < 1);

			for (int i = index - 1; i < userInput - 1; i++)
			{
				guildArray[i] = guildArray[i + 1];
			}
			userInput--;

			cout << endl;

			printMemberList(guildArray, userInput);

			_getch();
		}
		else if (userChoice)
		{
			active = 0;
		}
	}
}