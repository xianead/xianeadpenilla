#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>

using namespace std;

void sortArray(int arr[], int size)
{
	for (int i = 0; i <= size; i++)
	{
		for (int j = i + 1; j <= size - 1; j++)
		{
			if (arr[i] > arr[j])
			{
				swap(arr[i], arr[j]);
			}
		}
	}
}

void printArray(int arr[], int size)
{
	for (int i = 0; i < size; i++)
		cout << arr[i] << " ";
}

void fillArray(int arr[], int size, int max)
{
	for (int i = 0; i < size; i++)
		arr[i] = rand() % max + 1;

	sortArray(arr, size);
}

int binarySearch(int numArray[], int low, int high, int value, int steps = 0)
{
	int mid = (low + high) / 2;

	steps++;

	if (low > high)
	{
		return -1;
	}

	if (value == numArray[mid])
	{
		return steps;
	}
	else if (value < numArray[mid])
	{
		return binarySearch(numArray, low, mid - 1, value, steps);
	}
	else if (value > numArray[mid])
	{
		return binarySearch(numArray, mid + 1, high, value, steps);
	}
}

void reverseString(string& name, int x, int y)
{
	if (y > x)
	{
		return;
	}

	swap(name[y], name[x]);
	reverseString(name, x - 1, y + 1);
}

void printTriplets(int tripletArray[], int size, int x, int low, int high, int counter)
{
	if (low > high)
	{
		return;
	}

	if (tripletArray[low] + tripletArray[high] < x)
	{
		low++;
	}
	else if (tripletArray[low] + tripletArray[high] > x)
	{
		high--;
	}
	else
	{
		cout << "(" << tripletArray[counter] << ", " << tripletArray[low] << ", " << tripletArray[high] << ")" << endl;

		high--;
		low++;
	}

	printTriplets(tripletArray, size, x, low, high, counter++);
}

void findTriplets(int tripletArray[], int size, int sum, int counter = 0)
{
	if (counter > size)
	{
		return;
	}

	int x = sum - tripletArray[counter];

	int low = counter + 1;
	int high = size - 1;

	printTriplets(tripletArray, size, x, low, high, counter);
	findTriplets(tripletArray, size, sum, counter + 1);
}

void printBinarySearchResult(int value, int steps)
{
	cout << endl;

	if (steps != -1)
	{
		cout << value << " was found. Algorithm took " << steps << " step/s to find it." << endl;
	}
	else
	{
		cout << value << " was not found." << endl;
	}
}

void main()
{
	srand(time(NULL));

	string name;
	char userChoice;
	char decision;

	int sumInput;
	int tripletArrayLength;

	const int numArrSize = 10;
	const int tripletArrSize = 8;

	int numArray[numArrSize];
	int tripletArr[tripletArrSize];

	while (1)
	{
		cout << "What do you want to check, Sir Dale?\n" << endl;
		cout << "1. Reverse a given string. Must allow spaces." << endl;
		cout << "2. Find a triple with given sum in array." << endl;
		cout << "3. Perform binary search on a given sorted array.\n" << endl;
		cout << "Input: ";
		cin >> userChoice;

		system("cls");
		switch (userChoice)
		{
		case '1':
		{
			cin.ignore();
			cout << "Input: ";
			getline(cin, name);

			reverseString(name, name.length() - 1, 0);
			cout << "Output: " << name;

			_getch();
			system("cls");
		}
			break;
		case '2':
		{
			fillArray(tripletArr, tripletArrSize, 10);
			printArray(tripletArr, tripletArrSize);

			cout << "\n\nInput: ";
			cin >> sumInput;

			tripletArrayLength = sizeof(tripletArr) / sizeof(tripletArr[0]);
			findTriplets(tripletArr, tripletArrayLength, sumInput);

			_getch();
			system("cls");
		}
			break;
		case '3':
		{
			fillArray(numArray, numArrSize, 100);

			int numArrayLength = sizeof(numArray) / sizeof(numArray[0]);
			int value;

			printArray(numArray, numArrSize);

			cout << "\n\nWhich one do you want to search for? " << endl;
			cout << "Input: ";
			cin >> value;
			
			int low = 0;
			int high = numArrayLength - 1;
			int steps = 0;
			steps = binarySearch(numArray, low, high, value);

			printBinarySearchResult(value, steps);

			_getch();
			system("cls");
		}
			break;
		default:
			cout << "Try again, sir. Or do you want to end it? (y / n)" << endl;
			cout << "Input: ";
			cin >> decision;

			if (decision == 'y' || decision == 'Y')
			{
				system("cls");
				cout << "Bye, sir!" << endl;

				_getch();
				return;
			}
			else
			{
				cout << "Oki, sir!" << endl;
				return;
			}

			_getch();
			system("cls");
		}
	}		
	_getch();
}