#include <iostream>
#include <conio.h>

#include "Stack.h"
#include "Queue.h"

using namespace std;

void printTopElements(Stack<int>& stackSet, Queue<int>& queueSet)
{
	int queueSetTop = queueSet.top();
	int stackSetTop = stackSet.top();

	cout << "Top element of sets:" << endl;
	cout << "Queue: " << queueSetTop << endl;
	cout << "Stack: " << stackSetTop << endl;
}

void pushElements(Stack<int>& stackSet, Queue<int>& queueSet)
{
	int numToPush;

	cout << "\nEnter number: ";
	cin >> numToPush;

	stackSet.push(numToPush);
	queueSet.push(numToPush);
	printTopElements(stackSet, queueSet);
}

void popElements(Stack<int>& stackSet, Queue<int>& queueSet)
{
	stackSet.pop();
	queueSet.pop();

	cout << "\nYou've popped the top elements" << endl;

	printTopElements(stackSet, queueSet);
}

void popAllElements(Stack<int>& stackSet, Queue<int>& queueSet)
{
	int element;

	cout << "\n\nStack Elements: " << endl;
	while (!stackSet.isEmpty())
	{
		element = stackSet.top();
		cout << element << endl;
		stackSet.pop();
	}

	cout << "\n\nQueue Elements: " << endl;
	while (!queueSet.isEmpty())
	{
		element = queueSet.top();
		cout << element << endl;
		queueSet.pop();
	}
}

int main()
{
	cout << "Enter size for element sets: ";
	int elementSet;
	cin >> elementSet;

	Stack<int> stackSet(elementSet);
	Queue<int> queueSet(elementSet);

	system("cls"); 
	while (1)
	{
		char userInput;
		
		cout << "Size of element set: " << elementSet << endl;
		cout << "What do you want to do?" << endl;
		cout << "1 - Push element" << endl;
		cout << "2 - Pop element" << endl;
		cout << "3 - Print everything then empty set" << endl;
		cout << "\nYour answer: ";
		cin >> userInput;

		switch (userInput)
		{
		case '1': 
		{
			pushElements(stackSet, queueSet);
			_getch();
			system("cls");
		}
			break;
		case '2':
		{
			popElements(stackSet, queueSet);
			_getch();
			system("cls");
		}
			break;
		case '3':
		{
			popAllElements(stackSet, queueSet);
			_getch();
			system("cls");
		}
			break;
		default:
			cout << "\n\nTry again!";
			_getch();
			system("cls");
		}
	}

	return 0;
}