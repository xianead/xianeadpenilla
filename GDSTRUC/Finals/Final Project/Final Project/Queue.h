#pragma once

#include <assert.h>
#include <iostream>

using namespace std;

template<class T>

class Queue
{
public:

	Queue(int size, int growBy = 1) : mArray(NULL),
		mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~Queue()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T val)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		mArray[mNumElements] = val;
		mNumElements++;
	}
	
	const T& top()
	{
		assert(mArray != NULL);

		return mArray[0];
	}

	void pop()
	{
		if (mNumElements > 0)
		{
			for (int i = 0; i < mNumElements - 1; i++)
			{
				mArray[i] = mArray[i + 1];
			}
		}
		mNumElements--;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual bool isEmpty()
	{
		if (mNumElements != 0)
			return false;
		else
			return true;
	}

private:
	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;
	int mTop = 0;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};