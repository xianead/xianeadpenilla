#include <iostream>
#include <time.h>
#include <conio.h>

#include "UnorderedArray.h"
#include "OrderedArray.h"

using namespace std;

void printUnorederedList(UnorderedArray<int>& unordered)
{
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
}

void printOrderedList(OrderedArray<int>& ordered)
{
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";
}

int main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	int index, newSize;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (1)
	{
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		printUnorederedList(unordered);
		cout << "\nOrdered:   ";
		printOrderedList(ordered);

		char userInput;
		cout << "\n\nWhat do you want to do? " << endl;
		cout << "1 - Remove element at an index" << endl;
		cout << "2 - Search for element" << endl;
		cout << "3 - Expand and generate random values" << endl;
		cout << "Your answer: ";
		cin >> userInput;

		switch (userInput)
		{
		case '1':
			cout << "\n\nEnter index to remove: ";
			cin >> index;

			cout << "Element removed: " << index << endl;

			unordered.remove(index);
			ordered.remove(index);

			cout << "\nGenerated array: " << endl;
			cout << "Unordered: ";
			printUnorederedList(unordered);
			cout << "\nOrdered: ";
			printOrderedList(ordered);

			_getch();
			system("cls");
			break;
		case '2':
		{
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "\nOrdered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			_getch();
			system("cls");
		}
			break;
		case '3':

			cout << "Input size of expansion: ";
			cin >> newSize;
			
			for (int i = 0; i < newSize; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "\nGenerated array: " << endl;
			cout << "Unordered: ";
			printUnorederedList(unordered);
			cout << "\nOrdered:   ";
			printOrderedList(ordered);

			_getch();
			system("cls");
			break;
		}
		system("cls");
	}
	return 0;
}